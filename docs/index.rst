===============================
mgg
===============================

.. toctree::
   :maxdepth: 2

   README for mgg <README>
   CONTRIBUTING
   LICENSE
   CHANGELOG

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
