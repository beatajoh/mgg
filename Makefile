.PHONY: default, lint

default:
	vim tests/test_child_and_parents.py mgg/language.py
run:
	python -m mgg interactive
run_enterprise:
	python -m mgg enterprise ~/coa_gn.ini tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar "test" "PE test4"
spell:
	codespell . --ignore-words-list=hist --skip=./.* --quiet-level=2 || true
lint:
	pylint mgg 
pep8:
	autopep8 mgg  --in-place --recursive --aggressive --aggressive
clean:
	rm -rf build/ dist/ mgg_manager.egg-info/
test:
	codespell . --ignore-words-list=hist --quiet-level=2
	flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics
	pytest
reinstall:
	pip uninstall mgg 
	pyenv rehash
	pip install .
	pyenv rehash
