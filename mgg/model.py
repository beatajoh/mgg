# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg model Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

import json


def save_model_to_json(model: dict, filename: str) -> None:
    """
    Save a securicad model to a json file

    Arguments:
    model           - a SecuriCAD Model dictionary
                      this can be retrieved for example by:
                        model = model_info.get_model().model
                      or by using the mgg.securicad module function
                        `get_model`
    filename        - the name of the output file to be saved
    """
    with open(filename, 'w', encoding='utf-8') as file:
        json.dump(model, file, indent=4)


def load_model_from_json(filename: str) -> dict:
    """
    Load a securicad model from a json file

    Arguments:
    filename        - the name of the input file to parse
    """
    with open(filename, 'r', encoding='utf-8') as file:
        return json.load(file)


def get_objects(model: dict) -> dict:
    """
    Get all the objects present in an instance model.

    Arguments:
    model           - a SecuriCAD Model dictionary

    Return:
    A dictionary representing the whole set of objects
    contained in the instance model.
    The keys of this dictionary represent the IDs of the objects
    while the value associated to each key is a dictionary
    containing all the properties of the corresponding object
    """
    return model["objects"]


def get_links(model: dict) -> list:
    """
    Get all the links present in an instance model.

    Arguments:
    model           - a SecuriCAD Model dictionary

    Return:
    A list of dictionaries representing the whole set of connections
    between each object.
    Each element of the list (a dictionary) represents an edge connecting
    two objects in the instance model
    """
    return model["associations"]


def get_class(model: dict, object_id: str) -> str:
    """
    Get the class related to a specific object

    Arguments:
    model           - a SecuriCAD Model dictionary
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A string representing the class of the provided object ID
    """
    objects = get_objects(model)
    try:
        return objects[object_id]["metaconcept"]
    except KeyError as exc:
        print(exc)
        print(
            "Error: object ID or associated metaconcept non existing in the current model")
        return None


def get_links_for_object(model: dict, object_id: str) -> list:
    """
    Get the links associated to an object

    Arguments:
    model           - a SecuriCAD Model dictionary
    object_id       - the ID of the object we want to retrieve the class

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object.
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id in [association["id1"], association["id2"]]:
            assocs_for_entity.append(association)
    return assocs_for_entity


def get_links_for_object_by_class(
        model: dict, object_id: str, target_class: str
) -> list:
    """
    Get the links associated to an object connecting it to other objects
    only of a specific target class

    Arguments:
    model           - a SecuriCAD Model dictionary
    object_id       - the ID of the object we want to retrieve the links
    target_class    - a string representing the class of the objects
                      we want to see the connections to

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object with objects belonging to `target_class`.
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model belonging
    to `target_class`.
    """
    assocs = get_links(model)
    assocs_for_entity = []
    for association in assocs:
        if object_id in association["id1"]:
            other_object_class = get_class(model, association["id2"])
            if other_object_class == target_class:
                assocs_for_entity.append(association)
        elif object_id in association["id2"]:
            other_object_class = get_class(model, association["id1"])
            if other_object_class == target_class:
                assocs_for_entity.append(association)

    return assocs_for_entity



def get_associations_for_object(model: dict, obj_id: str) -> list:
    """
    Get the list of associations for a specific object

    Arguments:
    model           - a SecuriCAD Model dictionary
    object_id       - the ID of the object we want to retrieve the associations

    Return:
    A list of dictionaries representing the whole set of connections
    related to the provided object pointing inward (entering from the object).
    Each element of the list (a dictionary) represents an edge connecting
    the provided object to another object in the instance model
    """
    links = get_links_for_object(model, obj_id)
    assocs = []
    for link in links:
        if obj_id == link["id1"]:
            assocs.append({"id": link["id2"],
                           "type": link["type2"]})
        elif obj_id == link["id2"]:
            assocs.append({"id": link["id1"],
                           "type": link["type1"]})
    return assocs



def get_attacker_ids(model):
    atk_ids = []
    for objid,attribs in get_objects(model).items():
        if attribs['metaconcept'] == 'Attacker':
            atk_ids.append(objid)
    return atk_ids
