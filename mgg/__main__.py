# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate an attack graph from a MAL based SecuriCAD instance model
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
Main routine of mgg.

:Copyright: © 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""
from mgg.main import main


__all__ = ('main',)


if __name__ == '__main__':
    main()
