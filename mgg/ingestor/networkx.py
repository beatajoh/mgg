# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg networkx plugin Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

import networkx as nx


def ingest(graph: list) -> nx.DiGraph:
    g = nx.DiGraph()
    for node in graph:
        g.add_node(node.id, type=node.type, ttc=node.ttc)
        for link in node.links:
            g.add_edge(node.id, link)
    return g
