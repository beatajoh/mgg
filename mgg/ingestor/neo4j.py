# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg neo4j plugin Module

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

__all__ = ()

from py2neo import Graph, Node, Relationship, Subgraph
from securicad.model import Model, json_serializer, scad_serializer
from securicad.langspec import Lang


def ingest(graph: dict, 
        uri="bolt://localhost:7687",
        username="neo4j",
        password="mgg12345!",
        dbname="neo4j",
        delete=False
) -> None:
    """
    Ingest an attack graph into a neo4j database

    Arguments:
    graph                - the attackgraph provided by the atkgraph.py module.
                           Basically this is an array of AtkGraphNode structures
    uri                  - the URI to a running neo4j instance
    username             - the username to login on Neo4J
    password             - the password to login on Neo4J
    dbname               - the selected database
    delete               - if True, the previous content of the database is deleted
                           before ingesting the new attack graph
    """


    g = Graph(uri=uri, user=username, password=password, name=dbname)
    if delete:
        g.delete_all()

    nodes = {}
    rels = []

    for node in graph:
        nodes[node.id] = Node(node.objid,
                # Node(node.objid
                # Node(node.id
                name=node.id,
                type=node.type,
                objclass=node.objclass,
                objid=node.objid,
                atkname=node.atkname,
                is_traversable=node.is_traversable,
                is_reachable=node.is_reachable,
                graph_type="attackgraph",
                # ttc=node["ttc"],
                extra=node.extra or None)

    for node in graph:
        for link in node.links:
            # Reinsert the middle parameter "rel name"
            # Should i put objid instead of node.id? 
            rels.append(Relationship(nodes[node.id], nodes[link]))

    
    subgraph = Subgraph(list(nodes.values()), rels)

    tx = g.begin() 
    tx.create(subgraph)
    g.commit(tx)

        

def ingest_model(model: dict,
        uri="bolt://localhost:7687", 
        username="neo4j",
        password="mgg12345!",
        dbname="neo4j",
        delete=False
) -> None:
    """
    Ingest an instance model graph into a Neo4J database

    Arguments:
    model                - the instance model dictionary as provided by the model.py module
    uri                  - the URI to a running neo4j instance
    username             - the username to login on Neo4J
    password             - the password to login on Neo4J
    dbname               - the selected database
    delete               - if True, the previous content of the database is deleted
                           before ingesting the new attack graph
    """
    g = Graph(uri=uri, user=username, password=password, name=dbname)
    if delete:
        g.delete_all()

    nodes = {}
    rels = []

    for scad_id,attribs in model['objects'].items():
        nodeid = str(attribs['name']) + str(attribs['eid'])

        nodes[scad_id] = Node(attribs['metaconcept'],
                #attribs['metaconcept'],
                nodeid=nodeid,
                scad_id=scad_id,
                name=attribs['name'],
                graph_type="asset",
                metaconcept=attribs['metaconcept'],
                eid=attribs['eid'])

    for assoc in model['associations']:
        rels.append(Relationship(nodes[assoc['id1']], assoc['type1'], nodes[assoc['id2']]))
        rels.append(Relationship(nodes[assoc['id2']], assoc['type2'], nodes[assoc['id1']]))

    
    subgraph = Subgraph(list(nodes.values()), rels)

    tx = g.begin() 
    tx.create(subgraph)
    g.commit(tx)






def convert_model_to_mgg(uri="bolt://localhost:7687", 
        username="neo4j",
        password="mgg12345!",
        dbname="neo4j",
        delete=False
) -> dict:

    g = Graph(uri=uri, user=username, password=password, name=dbname) 

    # Get all relationships
    results = g.run('MATCH (a)-[r1]->(b),(a)<-[r2]-(b) WHERE NOT a.graph_type = "attackgraph" RETURN DISTINCT a, r1, r2, b').data()

    objects = {}
    associations = []

    for row in results:
        rel1 = list(row['r1'].types())[0]
        rel2 = list(row['r2'].types())[0]
        nodea = dict(row['a'])
        nodeb = dict(row['b'])
        objects[nodea['scad_id']] = {your_key: nodea[your_key] for your_key in ['name','metaconcept','eid']}
        objects[nodeb['scad_id']] = {your_key: nodeb[your_key] for your_key in ['name','metaconcept','eid']}
        add_elem = True
        for assoc in associations:
            if set([nodea['scad_id'],nodeb['scad_id'],rel1,rel2]) == set(assoc.values()):
                add_elem = False
        if add_elem:
            associations.append({'id1': nodea['scad_id'], 'id2': nodeb['scad_id'], 'type1': rel1, 'type2': rel2})


    newmodel = {'metadata': {'scadVersion': '1.0.0', 'langVersion': '0.3.0', 'langID': 'org.mal-lang.coreLang', 'malVersion': '0.1.0-SNAPSHOT', 'info': 'Created with securiCAD Professional 1.6'}, 'objects': objects, 'associations': associations}
    return newmodel




def convert_model_to_scad(language,
        output_scad_file="output.sCAD",
        uri="bolt://localhost:7687", 
        username="neo4j",
        password="mgg12345!",
        dbname="neo4j",
        delete=False
) -> None:

    newmodel = convert_model_to_mgg(uri, username, password, dbname, delete)

    scad_model = {"name": "unnamed", "meta": { "langId": newmodel['metadata']['langID'], "langVersion": newmodel['metadata']['langVersion']}}
    scad_model['objects'] = []
    scad_model['associations'] = []
    scad_model['views'] = []
    scad_model['icons'] = []

    for obj, values in newmodel['objects'].items():
        newdict = { "meta": {},
                    "id": abs(int(obj)),
                    "name": values['name'],
                    "asset_type": values['metaconcept'],
                    "attack_steps": [],
                    "defenses": []
                }
        scad_model['objects'].append(newdict)

    for assoc in newmodel['associations']:
        newdict = { "meta": {},
                    "target_object_id": abs(int(assoc['id1'])),
                    "target_field": assoc['type2'],
                    "source_object_id": abs(int(assoc['id2'])),
                    "source_field": assoc['type1']}
        scad_model['associations'].append(newdict)



    modelval = json_serializer.deserialize_model(scad_model, lang=language, validate_icons=False)

    modelval.create_view("default")

    # check for errors
    modelval.multiplicity_errors
    modelval.attacker_errors
    # or just:
    modelval.validate()

    scad_serializer.serialize_model(modelval, output_scad_file)
