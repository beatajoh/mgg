# -*- encoding: utf-8 -*-
# mgg v0.1.0
# Generate MAL-based attack graphs
# Copyright 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.

"""
mgg AtkGraphNode frozen dataclass

:Copyright: 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE).
"""

from typing import List, Optional
import attr
import dataclasses


@dataclasses.dataclass(frozen=False)
class AtkGraphNode:
    id: str
    type: str
    objclass: str
    objid: str
    atkname: str
    ttc: dict
    links: List[str]
    is_reachable: bool
    defense_status: dict
    graph_type: str
    is_traversable: bool = False
    required_steps: List[str] = None
    extra: dict = None

    def to_dict(self):
        return dataclasses.asdict(self)

