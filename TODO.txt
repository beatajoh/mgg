- 
- add test case with multiple parent steps belonging to class A with same name
  pointing to class B, it would be better if the test would not conflict with
  current tests

- check resolution of parent steps when parent is a complex step ()

- Test [] squared brackets attacks DONE 
- Test * squared brackets attacks  DONE
- Decouple build_external_links with other functions, e.g.:
    - resolve_child_with_associations ???
    - maybe: 
        forward_resolve_atkstep 
            should return something like:
                [(3292839283,"attackstep"),
                 (2323923821,"attackstep2"),
                 ...]
        backward_resolve_atkstep


Check that defenses also have steps... (ok)
and that they appear in the atkgraph (ok)
