# Return Model Graph
MATCH (n)  WHERE NOT n.graph_type = 'attackgraph' RETURN n


# Return Attack Graph
MATCH (n)  WHERE n.graph_type = 'attackgraph' RETURN n
