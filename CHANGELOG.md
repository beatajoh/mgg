# Changelog
:Info: This is the changelog for mgg.
:Author: Giuseppe Nebbione <nebbione@kth.se>
:Copyright: © 2021, Giuseppe Nebbione.
:License: GPLv3 (see /LICENSE or :doc:`Appendix B <LICENSE>`.)
:Date: 2021-11-08
:Version: 0.1.0



Version History
===============

0.1.0
    * Initial release
