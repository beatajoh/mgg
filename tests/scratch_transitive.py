import importlib
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from typing import List
from mgg.node import AtkGraphNode

importlib.reload(s)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)


# testLang4
model = s.load_model_from_scad_archive("tests/assets/testLang4/models/directories_branches-2-3.sCAD")
lang = s.load_language_specification("tests/assets/testLang4/org.mal-lang.testlang4-1.0.0.mar")
    

s.save_mar_to_json("tests/assets/testLang4/org.mal-lang.testlang4-1.0.0.mar","testLang4.json")
graph1 = a.generate_graph(lang, model)

lang_data = None
for asset in lang['assets']:
    if asset['name'] == 'Directory':
        lang_data = asset


atks = l.get_attacks_for_class(lang,"Directory")
l.get_child_steps(lang, "Directory", "writeDir")

graph1 = a.generate_graph(lang, model)

def build_links_from_transitive_attack_step(model, obj_id: str, attack_name: str) -> list:
    """
    Returns a list of strings representing node IDs in an attack graph.
    The format is: `assetId:atkname`

    Arguments:
    model           - a SecuriCAD Model object
                      this can be retrieved for example by:
                      model = model_info.get_model()
    obj_id          - the ID of the asset object
    attack_name     - a string representing an external attack name


    Return:
    A list of tuples in the format (ObjectID:AttackName, class).
    """
    complex_atk = attack_name.split(".")
    connections = []
    visited_objects = [obj_id]

    def _resolve(complex_atk, obj_id):
        obj_links = m.get_links_for_object(model, obj_id)
        if len(complex_atk) == 2:
            assoc = complex_atk[0]
            if "*" in assoc:
                assocname = assoc[:-1]
                for link in obj_links:
                    if obj_id == link["id1"] and link["type2"] == assocname:
                        next_obj_id = link["id2"]
                        if next_obj_id not in visited_objects:
                            visited_objects.append(next_obj_id)
                            obj_class = m.get_class(model, link["id1"])
                            connections.append(obj_class + ":" + link["id1"] + ":" + complex_atk[1])
                            _resolve(complex_atk, next_obj_id)
                    elif obj_id == link["id2"] and link["type1"] == assocname:
                        next_obj_id = link["id1"]
                        if next_obj_id not in visited_objects:
                            visited_objects.append(next_obj_id)
                            obj_class = m.get_class(model, link["id2"])
                            connections.append(obj_class + ":" + link["id1"] + ":" + complex_atk[1])
                            _resolve(complex_atk, next_obj_id)
    _resolve(complex_atk, obj_id)
    return connections
