import json
import importlib
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.ingestor.neo4j as mggneo
import mgg.atkgraph as a
from typing import List
from mgg.node import AtkGraphNode
import mgg.patterns as p
from py2neo import Graph, Node, Relationship, Subgraph

from securicad.model import Model, json_serializer, scad_serializer
from securicad.langspec import Lang


importlib.reload(s)
importlib.reload(p)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)
importlib.reload(mggneo)

model = s.load_model_from_scad_archive("tests/assets/coreLang/models/model_1_pattern.sCAD")
model = s.load_model_from_scad_archive("tests/assets/coreLang/models/model_2_pattern.sCAD")
lang  = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")

# Ingest Asset Graph
mggneo.ingest_model(model, delete=True)


# Ingest Attack Graph
graph = a.generate_graph(lang, model)
graph = a.attach_attacker(model, graph)
mggneo.ingest(graph, delete=False)


patterns = p.load_patterns("tests/assets/patterns2.json")
patterns = p.load_patterns("tests/assets/patterns2_valid.json")
target_class, target_atkname = "Application.fullAccess".split(".")

p.analyze_bad_patterns(model,patterns)


mitigation = patterns['nonSegmentedVulnApp']['mitigation']
p.apply_query(model, mitigation)


# Convert From Neo4J to mgg Json Format
newmodel = mggneo.convert_model_to_mgg(uri="bolt://localhost:7687", username="neo4j", password="mgg", dbname="neo4j", delete=False)

# Convert From Neo4J to .sCAD archive
language = Lang("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
mggneo.convert_model_to_scad(language)



##### Manual conversion from neo4j to python dictionary

uri="bolt://localhost:7687"
username="neo4j"
password="mgg"
dbname="neo4j"
delete=False
g = Graph(uri=uri, user=username, password=password, name=dbname) 

# Get all relationships
results = g.run('MATCH (a)-[r1]->(b),(a)<-[r2]-(b) WHERE NOT a.graph_type = "attackgraph" RETURN DISTINCT a, r1, r2, b').data()
# results = g.run('MATCH (a)-[r]-(b) WITH a,b,collect(r) as rels WHERE NOT a.graph_type = "attackgraph" RETURN a,rels,b').data()
# results = g.run('MATCH (n)-[r]->(m) WHERE NOT n.graph_type = "attackgraph" RETURN n,r,m').data()



objects = {}
associations = []


for row in results:
    rel1 = list(row['r1'].types())[0]
    rel2 = list(row['r2'].types())[0]
    nodea = dict(row['a'])
    nodeb = dict(row['b'])
    objects[nodea['scad_id']] = {your_key: nodea[your_key] for your_key in ['name','metaconcept','eid']}
    objects[nodeb['scad_id']] = {your_key: nodeb[your_key] for your_key in ['name','metaconcept','eid']}
    add_elem = True
    for assoc in associations:
        if set([nodea['scad_id'],nodeb['scad_id'],rel1,rel2]) == set(assoc.values()):
            add_elem = False
    if add_elem:
        associations.append({'id1': nodea['scad_id'], 'id2': nodeb['scad_id'], 'type1': rel1, 'type2': rel2})


newmodel = {'metadata': {'scadVersion': '1.0.0', 'langVersion': '0.3.0', 'langID': 'org.mal-lang.coreLang', 'malVersion': '0.1.0-SNAPSHOT', 'info': 'Created with securiCAD Professional 1.6'}, 'objects': objects, 'associations': associations}


scad_model = {"name": "unnamed", "meta": { "langId": newmodel['metadata']['langID'], "langVersion": newmodel['metadata']['langVersion']}}
scad_model['objects'] = []
scad_model['associations'] = []
scad_model['views'] = []
scad_model['icons'] = []

for obj, values in newmodel['objects'].items():
    newdict = { "meta": {},
                "id": abs(int(obj)),
                "name": values['name'],
                "asset_type": values['metaconcept'],
                "attack_steps": [],
                "defenses": []
              }
    scad_model['objects'].append(newdict)

for assoc in newmodel['associations']:
    newdict = { "meta": {},
                "target_object_id": abs(int(assoc['id1'])),
                "target_field": assoc['type2'],
                "source_object_id": abs(int(assoc['id2'])),
                "source_field": assoc['type1']}
    scad_model['associations'].append(newdict)


# Re-generate the attack graph (COMMENT IF NOT NEEDED)
mggneo.ingest_model(newmodel, delete=True)
graph = a.generate_graph(lang, newmodel)
graph = a.attach_attacker(newmodel, graph)
mggneo.ingest(graph, delete=False)


corelang = Lang("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
modelval = json_serializer.deserialize_model(scad_model, lang=corelang, validate_icons=False)

modelval.create_view("default")

# check for errors
modelval.multiplicity_errors
modelval.attacker_errors
# or just:
modelval.validate()

scad_serializer.serialize_model(modelval, "saved2.sCAD")











