import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from typing import List
from mgg.node import AtkGraphNode

        
model = s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")
lang = s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")
    


graph1 = a.generate_graph(lang, model)


a.save_atkgraph(graph1, filename="ciaopippo.json")

graph = a.load_atkgraph("ciaopippo.json")

d5andnodes = []
for node in graph:
    if node.atkname == "d5and":
    #if node["type"] == "and" and node["atkname"] == "d5and":
        required_parents = l.get_parent_steps(lang, node.objclass, node.atkname)
        print("NODE")
        print(node)
        d5andnodes.append(node)
        print("REQUIRES:")
        print(required_parents)
        print("NODE ACTUAL PARENTS:")
        print(a.get_node_parents(graph, node))
        for parent in required_parents:
            print(parent)





# SCENARIO 1
attached_nodes = ["AA:2245388199686721192:a3",
                  "AA:2245388199686721192:a4",
                  "AA:2245388199686721192:a7",
                  "AA:-214940586777624565:a3"]
graph = a.attach_attacker(lang, graph,attached_nodes)

conq = a.select(a.where(is_traversable=True),graph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),graph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),graph)
for node in reachable:
    print(node.id)

newgraph = a.prune_untraversable(graph)



newgraph = a.prune_unreachable(newgraph)
len(newgraph)

conq = a.select(a.where(is_traversable=True),newgraph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),newgraph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),newgraph)
for node in reachable:
    print(node.id)


model = s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")
lang = s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")
    


graph1 = a.generate_graph(lang, model)


a.save_atkgraph(graph1, filename="ciaopippo.json")

graph = a.load_atkgraph("ciaopippo.json")

# SCENARIO 2
attached_nodes = ["DD:-5122376439805427512:d6",
                  "DD:-5122376439805427512:d7"]

graph = a.attach_attacker(lang,graph,attached_nodes)

conq = a.select(a.where(is_traversable=True),graph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),graph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),graph)
for node in reachable:
    print(node.id)

newgraph = a.prune_untraversable(graph)
newgraph = a.prune_unreachable(newgraph)

conq = a.select(a.where(is_traversable=True),newgraph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),newgraph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),newgraph)
for node in reachable:
    print(node.id)


# SCENARIO 3
model = s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")
lang = s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")
    

graph = a.generate_graph(lang, model)
attached_nodes = ["AA:2245388199686721192:a1",
                  "AA:2245388199686721192:a2",
                  "AA:-214940586777624565:a1"]

graph = a.attach_attacker(lang,graph,attached_nodes)

conq = a.select(a.where(is_traversable=True),graph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),graph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),graph)
for node in reachable:
    print(node.id)

newgraph = a.prune_untraversable(graph)
newgraph = a.prune_unreachable(newgraph)
len(newgraph)

conq = a.select(a.where(is_traversable=True),newgraph)
for node in conq:
    print(node.id)

unreach = a.select(a.where(is_traversable=True, is_reachable=False),newgraph)
for node in unreach:
    print(node.id)

reachable = a.select(a.where(is_traversable=True, is_reachable=True),newgraph)
for node in reachable:
    print(node.id)



model = s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")
lang = s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")




# # Additional temp tests for parent identification
# # l.get_internal_parents(lang, "DD", "d8localAnd")
# # [elem in [("DD","d6"),("DD","d5"),("DD","d7")] for elem in l.get_internal_parents(lang, "DD", "d8localAnd")]
# # 
# # l.get_internal_parents(lang, "DD", "d5and")
# # l.get_external_parents(lang, "DD", "d5and")
# # l.get_parent_steps(lang, "DD", "d5and")
# 
n = a.select(a.where(node_id='DD:-5111312714521097079:d5and'),graph)[0]
reachable = a.is_and_node_reachable(lang, graph, n)


results = select(where(is_traversable=True, 
                       is_reachable=False),
                       graph)

results = select(where(node_id="DD:-5111312714521097079:d5and"),
                       graph)

results = select(where(cls="Application",
                       graph)

results = select(where(objid="5111312714521097079",
                       is_traversable=True,
                       graph)

results = select(where(atkname="write",
                       is_traversable=True,
                       cls="Application",
                       graph)
