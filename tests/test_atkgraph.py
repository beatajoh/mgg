#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# mgg test suite
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.


import pytest
import mgg
import mgg.model as m
import mgg.language as l
import mgg.securicad as s
import mgg.atkgraph as a


@pytest.fixture
def model():
    return s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")

@pytest.fixture
def lang():
    return s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")


def test_build_link_from_internal_attack_step(lang):
    obj_id = "7219598629313512" 
    atkstep = "fullAccess"
    cls = "Application"
    assert str(cls + ":" + obj_id + ":" + atkstep) == a.build_link_from_internal_attack_step(obj_id,atkstep, cls)

def test_build_links_from_external_attack_step(lang,model):
    ext_atkstep = "appSoftProduct.softProductVulnerabilities.localAccessAchieved"
    # Application which is then connected to 2 vulns
    obj_id = "7219598629313512" 
    links = a.build_links_from_external_attack_step(model, obj_id, ext_atkstep)
    expected_links = ['SoftwareVulnerability:892728578927324:localAccessAchieved',
                      'UnknownSoftwareVulnerability:929864580059290:localAccessAchieved']
    assert all(elem in expected_links for elem in links)


