#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# mgg test suite
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.


import pytest
import mgg
import mgg.model as m
import mgg.securicad as s



@pytest.fixture
def model():
    return s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")

@pytest.fixture
def lang():
    return s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")


def test_save_model_to_json(model):
    temp_path = "tests/assets/temp.json"
    m.save_model_to_json(model, temp_path)
    assert m.load_model_from_json(temp_path).keys() == model.keys()


def test_get_objects(model):
    assert len(m.get_objects(model)) == 8

def test_get_links(model):
    assert len(m.get_links(model)) == 7

def test_get_class(model):
    obj_net = "8176711980537409" # Network
    obj_app = "7219598629313512" # Application
    assert m.get_class(model, obj_net) == "Network"
    assert m.get_class(model, obj_app) == "Application"


def test_get_links_for_object(model):
    obj_net = "8176711980537409" # Network has three links
    obj_app = "7219598629313512" # Application

    len (m.get_links_for_object(model,obj_net)) == 3
    len (m.get_links_for_object(model,obj_app)) == 2

# def test_get_links_for_object_by_class()
# def test_get_associations_for_object()
