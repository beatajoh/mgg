import json
import importlib
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.ingestor.neo4j as mggneo
import mgg.atkgraph as a
from typing import List
from mgg.node import AtkGraphNode
import mgg.patterns as p
from py2neo import Graph, Node, Relationship, Subgraph


importlib.reload(s)
importlib.reload(p)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)
importlib.reload(mggneo)

model = s.load_model_from_scad_archive("tests/assets/coreLang/models/model_1_pattern.sCAD")
model = s.load_model_from_scad_archive("tests/assets/coreLang/models/model_2_pattern.sCAD")
# model = s.load_model_from_scad_archive("/home/gnc/Untitled.sCAD")
lang  = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
m.save_model_to_json(model, "model.json")

# Ingest Asset Graph
mggneo.ingest_model(model, delete=True)


# Ingest Attack Graph
graph = a.generate_graph(lang, model)
graph = a.attach_attacker(model, graph)
mggneo.ingest(graph, delete=False)


patterns = p.load_patterns("tests/assets/patterns2.json")
patterns = p.load_patterns("tests/assets/patterns2_valid.json")
target_class, target_atkname = "Application.fullAccess".split(".")

p.analyze_bad_patterns(model,patterns)


mitigation = patterns['nonSegmentedVulnApp']['mitigation']
badAtkGraphQuery = patterns['nonSegmentedVulnApp']['badAtkGraphPattern']
result = p.apply_query(model,badAtkGraphQuery)
for res in result:
    for path in res:
        nodes = res[path].nodes

# Find bad patterns by atkname specification
for node in nodes:
    if node['objclass'] == target_class and node['atkname'] == target_atkname:
        print("FOUND")

# This actually applies a mitigation
p.apply_query(model, mitigation)



##### From neo4j to python dictionary


uri="bolt://localhost:7687"
username="neo4j"
password="mgg"
dbname="neo4j"
delete=False
g = Graph(uri=uri, user=username, password=password, name=dbname) 

# Get all relationships
results = g.run('MATCH (a)-[r1]->(b),(a)<-[r2]-(b) WHERE NOT a.graph_type = "attackgraph" RETURN a, r1, r2, b').data()

objects = {}
associations = []

for row in results:
    rel1 = list(row['r1'].types())[0]
    rel2 = list(row['r2'].types())[0]
    nodea = dict(row['a'])
    nodeb = dict(row['b'])
    objects[nodea['scad_id']] = {your_key: nodea[your_key] for your_key in ['name','metaconcept','eid']}
    objects[nodeb['scad_id']] = {your_key: nodeb[your_key] for your_key in ['name','metaconcept','eid']}
    associations.append({'id1': nodea['scad_id'], 'id2': nodeb['scad_id'], 'type1': rel1, 'type2': rel2})


newmodel = {'metadata': {'scadVersion': '1.0.0', 'langVersion': '0.3.0', 'langID': 'org.mal-lang.coreLang', 'malVersion': '0.1.0-SNAPSHOT', 'info': 'Created with securiCAD Professional 1.6'}, 'objects': objects, 'associations': associations}


##### Now the attack graph is updated


# Re-generate the attack graph 
mggneo.ingest_model(newmodel, delete=True)
graph = a.generate_graph(lang, newmodel)
graph = a.attach_attacker(newmodel, graph)
mggneo.ingest(graph, delete=False)











#################################################################

# Temporary Test Code to run queries directly
results = g.run("""
        MATCH p1 =  (:ConnectionRule)-->(:Network)-->(:ConnectionRule)-->(:Application)<--(:UnknownSoftwareVulnerability)
        RETURN *""").data()


print_match('p1', results[0])
    

g.run("""
        MATCH p1 = (cn1:connectionrule)-->(n1:network)-->(cn2:connectionrule)-[r1]->(a1:application)<--(u1:unknownsoftwarevulnerability)
        DELETE r1
        CREATE (c1:credentials {name: "databasemitigation", id: "1111", type: "credentials"}),
               (c2:network {name: "networkmitigation", id: "2222", type: "network"}),
               (cn2)-[:assoc1]->(c1),
               (c1)-[:assoc2]->(c2),
               (c2)-[:assoc3]->(a1);
      """)


patterns = {'nonSegmentedVulnApp': 
        {'badPattern': """MATCH p1 = (v1:ConnectionRule)-->(:Network)-->(:ConnectionRule)-[r]->(:Application)<--(:UnknownSoftwareVulnerability)
                MATCH p2 = (v2:ConnectionRule)-->(:Network)-->(:ConnectionRule)-->(:Application)
                MATCH p3 = (v3:ConnectionRule)-->(:RoutingFirewall)
                WHERE v1 = v2 = v3 
                RETURN p1,p2,p3
                """,
         'mitigation': """MATCH p1 = (cn1:connectionrule)-->(n1:network)-->(cn2:connectionrule)-[r1]->(a1:application)<--(u1:unknownsoftwarevulnerability)
                DELETE r1
                CREATE (c1:credentials {name: "databasemitigation", id: "1111", type: "credentials"}),
                       (c2:network {name: "networkmitigation", id: "2222", type: "network"}),
                       (cn2)-[:assoc1]->(c1),
                       (c1)-[:assoc2]->(c2),
                       (c2)-[:assoc3]->(a1);
                """
        }
}



ingest_model(model, delete=True)

