import importlib
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from typing import List
from mgg.node import AtkGraphNode

importlib.reload(s)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)

# testLang5
model = s.load_model_from_scad_archive("tests/assets/testLang5/models/...")
lang = s.load_language_specification("tests/assets/testLang5/org.mal-lang.testlang5-1.0.0.mar")
    
l.get_attack_tree(lang)
l.save_language_specification(lang,"language.json")

s.save_mar_to_json("tests/assets/testLang5/org.mal-lang.testlang5-1.0.0.mar","testLang6.json")
graph1 = a.generate_graph(lang, model)

lang_data = None
for asset in lang['assets']:
    if asset['name'] == 'Computer':
        lang_data = asset


atks = l.get_attacks_for_class(lang,"Computer")
l.get_child_steps(lang, "Computer", "cameraExistence")

atks = l.get_attacks_for_class(lang,"Linux")
l.get_child_steps(lang, "Linux", "spyware")


# testLang6
model = s.load_model_from_scad_archive("tests/assets/testLang6/models/1OS_1Linux_Spyware_on_both.sCAD")
lang = s.load_language_specification("tests/assets/testLang6/org.mal-lang.testlang6-1.0.0.mar")
    

l.get_attack_tree(lang)
s.save_mar_to_json("tests/assets/testLang6/org.mal-lang.testlang6-1.0.0.mar","testLang6.json")
graph1 = a.generate_graph(lang, model)

lang_data = None
for asset in lang['assets']:
    if asset['name'] == 'OS':
        lang_data = asset


atks = l.get_attacks_for_class(lang,"OS")
l.get_child_steps(lang, "OS", "spyware")

atks = l.get_attacks_for_class(lang,"Linux")
l.get_child_steps(lang, "Linux", "spyware")

# testLang3        
model = s.load_model_from_scad_archive("tests/assets/testLang3/models/1_cable_2_traffic.sCAD")
lang = s.load_language_specification("tests/assets/testLang3/org.mal-lang.testlang3-1.0.0.mar")
    
atks = l.get_attack_tree(lang)

s.save_mar_to_json("tests/assets/testLang3/org.mal-lang.testlang3-1.0.0.mar","testLang3.json")
graph1 = a.generate_graph(lang, model)

variables = l.get_variables_for_class(lang,"Cable")
variables

# coreLang        

model = s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")
lang = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
    

l.get_attack_tree(lang)
l.save_language_specification(lang,"language.json")

s.save_mar_to_json("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar","coreLang.json")
graph1 = a.generate_graph(lang, model)

app_variables = l.get_variables_for_class(lang,"Application")
net_variables = l.get_variables_for_class(lang,"Network")


# testLang4
model = s.load_model_from_scad_archive("tests/assets/testLang4/models/directories_branches-2-3.sCAD")
lang = s.load_language_specification("tests/assets/testLang4/org.mal-lang.testlang4-1.0.0.mar")
    

s.save_mar_to_json("tests/assets/testLang4/org.mal-lang.testlang4-1.0.0.mar","testLang4.json")
graph1 = a.generate_graph(lang, model)







app_variables
net_variables

# MAL SPEC
# APPLICATION LET STMTS
#let allVulnerabilities = 
#   vulnerabilities \/ appSoftProduct.softProductVulnerabilities

#let allApplicationConnections = 
#   (appConnections \/ outgoingAppConnections \/ ingoingAppConnections)

# NETWORK LET STMTS
# let allNetConnections = 
#   netConnections \/ ingoingNetConnections \/ outgoingNetConnections \/ diodeIngoingNetConnections

#let allowedApplicationConnectionsApplications = 
#   (netConnections.applications \/ outgoingNetConnections.applications \/ applications)
let_allVars    = app_variables['allVulnerabilities']['stepExpression']
let_allAppConn = app_variables['allApplicationConnections']['stepExpression']
let_allNetConn = net_variables['allNetConnections']['stepExpression']
let_allowedApp = net_variables['allowedApplicationConnectionsApplications']['stepExpression']


let_allVars
let_allAppConn
let_allNetConn
let_allowedApp


def get_assets(asset_name):
    asset_name = asset_name.lower()

    if asset_name == "vulnerabilities.".lower():
        return ["vulnerabilities1", "vulnerabilities2"]

    elif asset_name == "appSoftProduct.softProductVulnerabilities.".lower():
        return ["appSoftProduct.softProductVulnerabilities",
                "appSoftProduct.softProductVulnerabilities2"]

    elif asset_name == "appConnections.".lower():
        return ["appConnections", "appConnections2"]

    elif asset_name == "outgoingAppConnections.".lower():
        return ["outgoingAppConnections", "outgoingAppConnections2"]

    elif asset_name == "ingoingAppConnections.".lower():
        return ["ingoingAppConnections", "ingoingAppConnections2"]

    elif asset_name == "netConnections.applications.".lower():
        return ["netConnections.applications", "netConnections.applications2"]

    elif asset_name == "netConnections.".lower():
        return ["netConnections", "netConnections2"]

    elif asset_name == "outgoingNetConnections.applications.".lower():
        return ["outgoingNetConnections.applications", "outgoingNetConnections.applications2"]

    elif asset_name == "applications.".lower():
        return ["applications", "applications2"]

    elif asset_name == "ingoingNetConnections.".lower():
        return ["ingoingNetConnections", "ingoingNetConnections2"]

    elif asset_name == "outgoingNetConnections.".lower():
        return ["outgoingNetConnections", "outgoingNetConnections2"]

    elif asset_name == "diodeIngoingNetConnections.".lower():
        return ["diodeIngoingNetConnections", "diodeIngoingNetConnections2"]
    else: 
        return ["else"]




def buildexpr(rlhs):
    def get_asset_name(namexp):
        expr = ""

        def _get_name(namexp):
            if namexp["type"] == "field":
                return expr + namexp["name"] + "."
            # If it is a complex nested name
            elif namexp["type"] == "collect":
                return expr + \
                    _get_name(namexp["lhs"]) + _get_name(namexp["rhs"])
        return _get_name(namexp)

    def _buildexpr(rlhs):
        # If it is a union operation
        if rlhs["type"] in ["field","collect"]:
            return set(get_assets(get_asset_name(rlhs)))
        elif rlhs["type"] == "union":
            return _buildexpr(rlhs['lhs']) | _buildexpr(rlhs['rhs'])
        elif rlhs["type"] == "intersection":
            return _buildexpr(rlhs['lhs']) & _buildexpr(rlhs['rhs'])

        elif rlhs["type"] == "difference":
            return _buildexpr(rlhs['lhs']) - _buildexpr(rlhs['rhs'])
    return _buildexpr(rlhs)

let_allVars
a = buildexpr(let_allVars)
a

let_allAppConn
a = buildexpr(let_allAppConn)
a

a = buildexpr(let_allNetConn)
a

a = buildexpr(let_allowedApp)
a

# Modified allApplicationConnections
let_allAppConn["lhs"]["type"] = "intersection"
let_allAppConn
a = buildexpr(let_allAppConn)
a


# Modified allApplicationConnections
let_allAppConn["lhs"]["type"] = "difference"
let_allAppConn
a = buildexpr(let_allAppConn)
a
#let allVulnerabilities = 
#   vulnerabilities \/ appSoftProduct.softProductVulnerabilities

#let allApplicationConnections = 
#   (appConnections \/ outgoingAppConnections \/ ingoingAppConnections)

# NETWORK LET STMTS
# let allNetConnections = 
#   netConnections \/ ingoingNetConnections \/ outgoingNetConnections \/ diodeIngoingNetConnections

#let allowedApplicationConnectionsApplications = 
#   (netConnections.applications \/ outgoingNetConnections.applications \/ applications)


# we want a function that when given a relationship name is able to find all
# assets related to that relationship find_objects_in_rel(lang, model, "inTraffic")
# this would return something like:
#  object_ID1
#  object_ID2

# At this point for each field in the variable name we would like to have a
# dictionary with an associated list of objects...

# Then we traverse the variable again



# Get Data Infomation


model = s.load_model_from_scad_archive("tests/assets/coreLang/models/petest4.sCAD")
lang = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")

lang_data = None
for asset in lang['assets']:
    if asset['name'] == 'Data':
        lang_data = asset


atks = l.get_attacks_for_class(lang,"Data")
l.get_child_steps(lang, "Data", "overrideCredentials")

l.get_child_steps(lang, "Credentials", "propagateOneCredentialCompromised")

m.save_model_to_json(model, "model.json")

