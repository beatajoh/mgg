#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# mgg test suite
# Copyright © 2021, Giuseppe Nebbione.
# See /LICENSE for licensing information.


import pytest
import mgg
import mgg.model as m
import mgg.language as l
import mgg.securicad as s



@pytest.fixture
def model():
    return s.load_model_from_scad_archive("tests/assets/testLang2/models/2A_1B_1C_2D.sCAD")

@pytest.fixture
def lang():
    return s.load_language_specification("tests/assets/testLang2/org.mal-lang.testlang2-1.0.0.mar")


def test_get_objects(model):
    assert len(m.get_objects(model)) == 7

def test_get_links(model):
    assert len(m.get_links(model)) == 9


def test_get_child_steps(lang):
    assert l.get_child_steps(lang,"AA", "a3") == ['b.c.c2']
    assert l.get_child_steps(lang,"BB", "b1") == ['b4']


def test_get_internal_parents(lang):
    assert len(l.get_internal_parents(lang, "DD", "d8localAnd")) == 2
    assert all(elem in [("-","d6"),("-","d7")] for elem in l.get_internal_parents(lang, "DD", "d8localAnd"))

def test_get_external_parents(lang):
    assert len(l.get_external_parents(lang, "CC", "c3")) == 1
    assert all(elem in [("BB","b2")] for elem in l.get_external_parents(lang, "CC", "c3"))
    assert len(l.get_external_parents(lang, "DD", "d6")) == 1
    assert all(elem in [("AA","a8")] for elem in l.get_external_parents(lang, "DD", "d6"))


def test_get_parent_steps(lang):
    assert all(elem in [("-",'d6'),("-",'d7')] for elem in l.get_parent_steps(lang, "DD", "d8localAnd"))
    assert len(l.get_parent_steps(lang, "DD", "d5and")) == 4
    assert all(elem in [("-","d1"),("-","d2")] for elem in l.get_internal_parents(lang, "DD", "d5and"))
    assert all(elem in [("AA","a7"),("CC","c3")] for elem in l.get_external_parents(lang, "DD", "d5and"))
    assert all(elem in [("AA","a7"),("CC","c3"),("-","d2"),("-","d1")] for elem in l.get_parent_steps(lang, "DD", "d5and"))
