import importlib
import json
import zipfile
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from typing import List
import xml.etree.ElementTree as ET
from mgg.node import AtkGraphNode

importlib.reload(s)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)


# coreLang        

model = s.load_model_from_scad_archive("tests/assets/coreLang/models/atk_enabled_defenses.sCAD")
lang = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")



m.get_objects(model)
def get_attacker_id(model):
    atk_ids = []
    for objid,attribs in m.get_objects(model).items():
        if attribs['metaconcept'] == 'Attacker':
            atk_ids.append(objid)
    return atk_ids



atk_ids = get_attacker_id(model)
atk_assocs = m.get_links_for_object(model,atk_id)


def attach_attacker_from_model(model,attacker_id):
    atk_assocs = m.get_links_for_object(model,atk_id)
    for assoc in atk_assocs:
        if assoc['id1'] == atk_id:
            atkname = assoc['type1'].split(".")[0]
            target_id = assoc['id2']
            target_class = m.get_class(model, assoc["id2"])


