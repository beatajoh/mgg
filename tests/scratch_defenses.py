import importlib
import json
import zipfile
import securicad
import mgg.securicad as s
import mgg.language as l
import mgg.model as m
import mgg.atkgraph as a
from typing import List
import xml.etree.ElementTree as ET
from mgg.node import AtkGraphNode

importlib.reload(s)
importlib.reload(l)
importlib.reload(m)
importlib.reload(a)


# coreLang        

model = s.load_model_from_scad_archive("tests/assets/coreLang/models/atk_enabled_defenses.sCAD")
lang = s.load_language_specification("tests/assets/coreLang/org.mal-lang.coreLang-0.3.0.mar")
scad_archive = "tests/assets/coreLang/models/atk_enabled_defenses.sCAD"


m.get_objects(model)



model = {}

with zipfile.ZipFile(scad_archive, 'r') as archive:
    filelist = archive.namelist()
    model_file = next(filter(lambda x: ( x[-4:] == '.eom'), filelist))
    meta_file = next(filter(lambda x: (x == 'meta.json'), filelist))
    model["metadata"] = json.loads(archive.read(meta_file))
    scad_model = archive.read(model_file)
    root = ET.fromstring(scad_model)

objects_dict = {}
assocs = []
full_dict = {}

for child in root:
    print(child.tag, child.attrib)


for child in root.iter("objects"):
    objects_dict[child.attrib['id']] = { 
            "name": child.attrib['name'],
            "metaconcept": child.attrib['metaConcept'],
            "eid": child.attrib['exportedId']}



objects_dict = {}
for child in root.iter("objects"):
    objid = child.attrib["id"]
    objects_dict[objid] = { 
            "name": child.attrib['name'],
            "metaconcept": child.attrib['metaConcept'],
            "eid": child.attrib['exportedId']}
    for subchild in child.iter("evidenceAttributes"):
        defense_name = subchild.attrib['metaConcept']
        for distrib in subchild.iter("evidenceDistribution"):
            distrib_type = distrib.attrib['type']
            for d in distrib.iter("parameters"):
                if 'value' in d.attrib:
                    dist_value = d.attrib['value']
                    if 'active_defense' not in objects_dict[objid]:
                        objects_dict[objid]['active_defense'] = {}
                    objects_dict[objid]['active_defense'][defense_name] = {'distribution': distrib_type, 'value': dist_value}


                                 




            # for param  in distrib:
            #     print("THIS HAS A PARAMETER")
            #     print(param)
        # if subchild.find('evidenceDistribution'):
        #     print(subchild.find('parameters'))


for child in root.iter("associations"):
    assocs.append({"id1": child.attrib['sourceObject'],
                "id2": child.attrib['targetObject'],
                "type1": child.attrib['targetProperty'],
                "type2": child.attrib['sourceProperty']})

model["associations"] = assocs
model["objects"] = objects_dict


